package main

import (
	"fmt"
)

type IPoint interface {
	DistanceToOrigin() float64
}

type specialPoint struct {
	x float64
}

func (p *specialPoint) DistanceToOrigin() float64 {
	return p.x * p.x
}

func main() {
	list := make([]int, 4, 6)

	list = append(list, 1)
	list = append(list, 2)
	list = append(list, 3)
	list = append(list, 4)

	fmt.Println(list)

	dict := make(map[string]int)

	dict["Jan"] = 1
	dict["Feb"] = 2
	dict["Mar"] = 3

	fmt.Println(dict)
	fmt.Println(dict["Jan"])

	for key, value := range dict {
		fmt.Println(key, value)
	}

	p := specialPoint{3.0}
	fmt.Println(p.DistanceToOrigin())
}
