package main

import (
	"fmt"
	"math/rand"

	"gitlab.com/akita/akita/v3/sim"
)

type CellSplitEvent struct {
	time    sim.VTimeInSec
	handler sim.Handler
}

func (e CellSplitEvent) Time() sim.VTimeInSec {
	return e.time
}

func (e CellSplitEvent) Handler() sim.Handler {
	return e.handler
}

func (e CellSplitEvent) IsSecondary() bool {
	return false
}

type CellSplitEventHandler struct {
	engine    sim.EventScheduler
	cellCount int
}

func (h *CellSplitEventHandler) Handle(e sim.Event) error {
	cellSplitEvent, ok := e.(CellSplitEvent)
	if !ok {
		panic("wrong event type")
	}

	h.cellCount++
	fmt.Printf("Cell split at time %f, cell count: %d\n",
		cellSplitEvent.time, h.cellCount)

	// Randomly generate a number between 1-2
	timeUntilNextSplit := sim.VTimeInSec(rand.Float64() + 1)
	event := CellSplitEvent{
		time:    cellSplitEvent.time + timeUntilNextSplit,
		handler: h,
	}

	// Stop at time 200
	if cellSplitEvent.time < 200 {
		h.engine.Schedule(event)
	}

	return nil
}

func main() {
	engine := sim.NewSerialEngine()
	handler := &CellSplitEventHandler{
		cellCount: 1,
		engine:    engine, // Dependency injection, design pattern
	}

	event := CellSplitEvent{
		time:    0,
		handler: handler,
	}
	engine.Schedule(event)

	engine.Run()
}
