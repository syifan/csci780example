package math

// Point represents a point in 2D space.
type Point struct {
	x, y float64
}

// NewPoint creates a new point.
func NewPoint(x, y float64) *Point {
	return &Point{x, y}
}

// DistanceToOrigin returns the distance of the point to the origin.
func (p Point) DistanceToOrigin() float64 {
	return p.x*p.x + p.y*p.y
}

// Double doubles the point.
func (p *Point) Double() {
	p.x *= 2
	p.y *= 2
}
